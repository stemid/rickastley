from flask import Flask, jsonify

app = Flask(__name__)


@app.route('/', methods=['POST'])
def return_song():
    return jsonify({
        'play': 'https://rickastley.s3.eu-north-1.amazonaws.com/Rick_Astley-Never_Gonna_Give_You_Up.mp3',
        'skippable': False
    })


if __name__ == '__main__':
    app.run()
