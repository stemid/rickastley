# National Rick Astley hotline

>**NOTE** this repo has been archived and the function is taken over by [this repo](https://gitlab.com/stemid/joke-phone-number.git).

Inspired by [this repo](https://github.com/pjf/rickastley) here is the Swedish version.

* 🇸🇪 0766 86 84 36

**Disclaimer:** This project has no other aim than to bring joy to people. The cost to call the number from within Sweden should be standard outgoing call costs. I am not being payed anything, I am also not paying much to receive calls. Only about 9 SEK/month to reserve the number.

# Call log graph

![Call events over time](https://rickastley.s3.eu-north-1.amazonaws.com/graph.png "Call events over time")

# Technical details

* [46elks](https://46elks.se/) for the front-end number.
* Flask for running a small lambda webhook.
* Zappa for deploying Flask to AWS Lambda.

## Run

* Setup a virtualenv
* Install dependencies

```
$ python3.10 -m venv .zappa-env
$ source .zappa-env/bin/activate
$ pip install -r requirements.zappa.txt
$ python rickroll.py
```

## Deploy to AWS with zappa

```
$ zappa init
$ zappa deploy
```

Use the URL as a webhook for your 46elks number.

# Generate graph of call log

    $ python3 -m venv .venv
    $ source .venv/bin/activate
    $ pip install -r requirements.txt
    $ python callsgraph.py --save --user '<46elks API username>' --password '<46elks API password>'
    $ aws s3 cp --acl public-read graph.png s3://rickastley/graph.png
