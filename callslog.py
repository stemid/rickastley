import sys
from pprint import pprint as pp
from datetime import datetime, timedelta

import click
import requests


@click.command()
@click.option(
    '--user',
    required=True,
    type=str
)
@click.option(
    '--password',
    required=True,
    type=str
)
def show_callslog(user, password):
    end_time = datetime.now() - timedelta(days=365)

    # Request JSON from API
    r = requests.get(
        'https://api.46elks.com/a1/sms',
        auth=(user, password),
        params={
            'end': end_time.isoformat(),
            'start': datetime.now().isoformat(),
            'limit': 500
        }
    )

    try:
        sms = r.json()
    except Exception as e:
        print(str(e))
        print(r.text)
        sys.exit(-1)

    if not len(sms):
        print('No sms returned')
        sys.exit(-1)

    pp(sms)


if __name__ == '__main__':
    show_callslog()
